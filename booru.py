from pybooru import Danbooru
from pybooru import exceptions as booruExceptions
from requests import exceptions as requestsExceptions
from db import Botdb
from config import ConfigReader
import requests
import logging
import time

conf = ConfigReader('config.yml')

logs = logging.getLogger('Commands')

db = Botdb(conf.get('db_file'))

class Booru:
    BASE_URL = "https://danbooru.donmai.us"

    def __init__(self) -> None:
        self.client = Danbooru('danbooru')

    def get_pic_urls(self, post_limit: int) -> list:
        try:
            time.sleep(0.01)
            posts = self.client.post_list(limit=post_limit)
        except (booruExceptions.PybooruHTTPError, requestsExceptions.ConnectionError) as e:
            return []

        file_list = [
            {'Tags': [tag for tag in post['tag_string'].split(' ')], 'PostURL': f"https://danbooru.donmai.us/posts/{post['id']}"} if 'id' in post else None for post in posts
        ]

        return file_list

    def get_tags_by_id(self, id: int):
        return [tag for tag in self.client.post_show(id)['tag_string'].split(' ')]

    def search_tags_by_name(self, name: str) -> dict:
        """finds tags and alternatives (if they exist) based on a given name  

        :param name: the name to search for  
        :type name: str  
        :return: dictionary of matches and alternatives  
        :rtype: dict
        """    
        categories = {0:'general', 1:'artist', 3:'work', 4:'character'}
        tags = {}
        response = self.client.tag_list(name_matches=name, hide_empty=True)
        alts = self.client.tag_aliases(name_matches=name)
        if response:
            tags['matches'] = [{'id': tag['id'], 'name': tag['name'], 'category': categories[tag['category'], 'count': tag['tag_count']]} for tag in response]
        if alts:
            tags['alternatives'] = [{'id': tag['id'], 'name': tag['name'], 'category': categories[tag['category'], 'count': tag['tag_count']]} for tag in alts]
        return tags

    def get_latest_posts(self, tags="", limit=1) -> list:
        """gets posts with optional tags  

        :param tags: a string of space separated tags to query. max 2  
        :type tags: string  
        :param limit: how many posts to fetch
        :type limit: int 
        :raises ValueError: if more than 2 tags are supplied  
        :return: list of post data  
        :rtype: list
        """  
        tag_list = tags.split(" ")
        if len(tag_list) > 2:
            raise ValueError("number of tags cannot exceed 2")
        elif limit > 10:
            raise ValueError("limit can not be greater than 10")
        else:
            url_action = "posts.json"
            url = f"{self.BASE_URL}/{url_action}"
            response = requests.get(url, {"limit": limit, "tags": tag_list})
            return [{"url": file.get('file_url', file.get('source', None)), "tags": [tag for tag in file.get('tag_string', []).split(' ')]} for file in response.json()]

    def get_random_post(self, tags="") -> dict:
        """gets a random post with optional tags  

        :param tags: a string of space separated tags to query. max 2  
        :type tags: string  
        :raises ValueError: if more than 2 tags are supplied  
        :return: dict of data about the fetched post  
        :rtype: dict
        """  
        tag_list = tags.split(" ")
        if len(tag_list) > 2:
            raise ValueError("number of tags cannot exceed 2")
        else:
            url_action = "posts/random.json"
            url = f"{self.BASE_URL}/{url_action}"
            response = requests.get(url, {"tags": tag_list})
            file = response.json()
            return {"url": file.get('file_url', file.get('source', None)), "tags": [tag for tag in file.get('tag_string', []).split(' ')]}