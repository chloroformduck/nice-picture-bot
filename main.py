import logging
from interactions import Client, Intents, MISSING
from config import ConfigReader

conf = ConfigReader('config.yml')
environ = conf.get('environment')
token = conf.get('discord_token')
guild = conf.get('guild')
log_file_name = conf.get('log_file_name')
if not guild:
    guild = MISSING

if environ == 'prod':
    loglevel = "warning"
else:
    loglevel = "info"
numeric_level = getattr(logging, loglevel.upper(), None)
logging.basicConfig(format='%(asctime)s|%(levelname)s|%(name)s:  %(message)s', filename=log_file_name, level=numeric_level, datefmt='%Y-%m-%d %H:%M:%S')

def main():
    bot = Client(default_scope=guild, intents=Intents.DEFAULT)
    bot.load_extension("commands")
    bot.start(token)

if __name__ == '__main__':
    main()
