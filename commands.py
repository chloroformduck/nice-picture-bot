import logging
from db import Botdb
from booru import Booru
from interactions import Extension, SlashContext, SlashCommandOption,\
    OptionType, slash_command

from config import ConfigReader

conf = ConfigReader('config.yml')

logs = logging.getLogger('Commands')

db = Botdb(conf.get('db_file'))

class Commands(Extension):
    "Extension that handles the commands"

    def __init__(self, bot) -> None:
        self.booru = Booru()

    def _check_tags_from_post(self, url_tags, check_tags) -> bool:
        "checks if tags in the first list are in the second list"
        return any(i in url_tags for i in check_tags)

    @slash_command(
        name="post",
        sub_cmd_name="latest",
        sub_cmd_description="Post latest pictures with optionally up to two tags",
        options=[
            SlashCommandOption(
                type=OptionType.STRING,
                name="tags",
                required=False,
                description="Up to two tags to use in the search, separated by a space"
            ),
            SlashCommandOption(
                type=OptionType.INTEGER,
                name="limit",
                required=False,
                description="The number of images to post with this search"
            )
        ]
    )
    async def _post_latest(self, ctx: SlashContext, tags = "",limit = 1):
        if not ctx.channel.nsfw:
            await ctx.send("Oops, can't send that in a Christian channel! Please try again in a channel marked NSFW.", ephemeral=True)
            return
        try:
            results = self.booru.get_latest_posts(tags=tags, limit=limit)
        except ValueError as e:
            await ctx.send(f"An error ocurred with your search: {e}", ephemeral=True)
            return
        if len(results) == 0:
            suggestions = []
            await ctx.defer(ephemeral=True)
            for tag in tags.split(" "):
                for alt in self.booru.search_tags_by_name(tag).get('alternatives', []):
                    suggestions.append((alt['name'],alt['count'])) if alt['count'] > 0 else None
            if len(suggestions) > 0:
                l = [f"Tag Name: {t[0]} Total Posts: {t[1]}" for t in suggestions]
                await ctx.send(f"There weren't any results found for your search. Perhaps one of these tags would be better:\n{", ".join(l)}", ephemeral=True)
            else:
                await ctx.send("I couldn't find any posts for the tags you provided, and I wasn't able to find any suggested alternative tags either. Please try again with different tags. Sorry!", ephemeral=True)
        else:
            banned_count = 0
            banned_list = db.get_banned_tags(ctx.guild_id)
            for result in results:
                if self._check_tags_from_post(result['tags'], banned_list):
                    banned_count += 1
                else:
                    await ctx.send(result['url'])
            if banned_count == len(results):
                await ctx.send(f"Unfortunately, all the posts I got had tags that were on the banned tags list for this server\nTry increasing the limit of posts to check\nThe current banned tags list for this server is:\n{", ".join(banned_list)}", ephemeral=True)
            elif banned_count > 0:
                await ctx.send(f"Out of the results returned, {banned_count} contained tags on the banned tags list for this server", ephemeral=True)
                
    @slash_command(
        name="post",
        sub_cmd_name="random",
        sub_cmd_description="Post a random picture with optionally up to two tags",
        options=[
            SlashCommandOption(
                type=OptionType.STRING,
                name="tags",
                required=False,
                description="Up to two tags to use in the search, separated by a space"
            )
        ]
    )
    async def _post_random(self, ctx: SlashContext, tags = ""):
        if not ctx.channel.nsfw:
            await ctx.send("Oops, can't send that in a Christian channel! Please try again in a channel marked NSFW.", ephemeral=True)
            return
        try:
            result = self.booru.get_random_post(tags=tags)
            tries = 0
            while self._check_tags_from_post(result['tags'], db.get_banned_tags(ctx.guild_id)):
                if tries == 4:
                    await ctx.defer()
                if tries > 8:
                    await ctx.send("Unfortunately, I was not able to find a post without a banned tag. Please try again, perhaps with different tags", ephemeral=True)
                result = self.booru.get_random_post( tags=tags)
                tries += 1
        except ValueError as e:
            await ctx.send(f"An error ocurred with your search: {e}", ephemeral=True)
            return
        if not result.get('url'):
            suggestions = []
            await ctx.defer(ephemeral=True)
            for tag in tags.split(" "):
                for alt in self.booru.search_tags_by_name(tag).get('alternatives', []):
                    suggestions.append((alt['name'],alt['count'])) if alt['count'] > 0 else None
            if len(suggestions) > 0:
                l = [f"Tag Name: {t[0]} Total Posts: {t[1]}" for t in suggestions]
                await ctx.send(f"There weren't any results found for your search. Perhaps one of these tags would be better:\n{", ".join(l)}", ephemeral=True)
            else:
                await ctx.send("I couldn't find any posts for the tags you provided, and I wasn't able to find any suggested alternative tags either. Please try again with different tags. Sorry!", ephemeral=True)
        else:
            await ctx.send(result['url'])

    @slash_command(
        name="ban_tags",
        description="Images with banned tags will never be posted in this server",
        options=[
            SlashCommandOption(
                name="tags",
                description="Space-separated list of tags to ban",
                type=OptionType.STRING
            )
        ]
    )
    async def _ban_tags(self, ctx: SlashContext, tags: str):
        current_banned = db.get_banned_tags(ctx.guild_id)
        tags_to_ban = tags.split(" ")
        if len(tags_to_ban) > 4:
            await ctx.defer(ephemeral=True)
        to_ban = [tag if tag not in current_banned else None for tag in tags_to_ban]
        new_banned_tags = db.add_banned_tags(to_ban, ctx.guild_id)
        await ctx.send(f"Successfully updated banned tags list. The currently banned tags for this server are now:\n{",".join(new_banned_tags)}", ephemeral=True)

    @slash_command(
        name="unban_tags",
        description="Unbans tags from this server",
        options=[
            SlashCommandOption(
                name="tags",
                description="Space-separated list of tags to unban",
                type=OptionType.STRING
            )
        ]
    )
    async def _unban_tags(self, ctx: SlashContext, tags: str):
        current_banned = db.get_banned_tags(ctx.guild_id)
        tags_to_unban = tags.split(" ")
        if len(tags_to_unban) > 4:
            await ctx.defer(ephemeral=True)
        to_unban = [tag if tag in current_banned else None for tag in tags_to_unban]
        new_banned_tags = db.remove_banned_tags(to_unban, ctx.guild_id)
        await ctx.send(f"Successfully updated banned tags list. The currently banned tags for this server are now:\n{",".join(new_banned_tags)}", ephemeral=True)

    @slash_command(name="check_settings", description="Prints the current settings for this server")
    async def _check_settings(self, ctx: SlashContext):
        current_banned = db.get_banned_tags(ctx.guild_id)
        await ctx.send(f"Current Settings\nBanned Tags: {", ".join(current_banned)}", ephemeral=True)