import logging
import os
import yaml

logs = logging.getLogger("Config")

class ConfigReader():
    """Helper class for reading config options from environment variables or the config file.
    Prioritizes looking for an environment variable, and if not found will fall back to the config file if present.

    Attributes:
        file_path -- an optional path to a config file in yml format
    """    
    def __init__(self, file_path='config.yml') -> None:
        try:
            with open(file_path, 'r') as doc:
                self.config = yaml.safe_load(doc)
        except FileNotFoundError as e:
            logs.info(f"Config file not found: {e}")
            self.config = {}

    def get(self, key: str, default=None) -> str:
        """Read a config key and returns the value or default if the key isn't found

        :param key: Key to look for
        :type key: str
        :param default: The default value if the key isn't found. defaults to None
        :type default: Any
        :return: Value of the key or default
        :rtype: Union[str, int, None]
        """        
        if os.getenv(key):
            return os.getenv(key)
        else:
            try:
                return self.config[key]
            except AttributeError:
                logs.info("No environment variable found with the specified key and config file not found")
                return default
            except KeyError:
                logs.info(f"{key} was not found in the loaded config file")
                return default