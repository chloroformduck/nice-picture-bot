A discord bot for posting nice pictures.  
  
Set up: `pip install -r requirements.txt`  
  
Run: `python main.py <log level>`  
  
You'll need a config.yml with some configuration options, rename config.yml.sample to config.yml and enter the proper data for your use.