import logging
import yaml
from pathlib import Path
from pprint import pformat

logs = logging.getLogger('DB')

class Botdb():
    "Class to handle database functions in my discord bots"
    def __init__(self, db_file) -> None:
        self.db_file = Path(db_file)
        if self.db_file.is_file():
            with open(self.db_file, 'r') as f:
                self.data_stream = {doc['type']: doc['data'] for doc in yaml.safe_load_all(f)}
                logs.info("Loaded Datastream")
                logs.debug(pformat(self.data_stream))
        else:
            self.data_stream = {}

    def _save(self) -> None:
        "Saves the currently loaded data stream back to the file"
        data = [{"type": k, "data": v} for k,v in self.data_stream.items()]
        with open(self.db_file, "w") as f:
            yaml.safe_dump_all(data, f)

    def _iterate_keys(self, keys: list) -> None:
        "Adds a series of nested dictionaries to the data_stream"
        selected = self.data_stream
        for k in keys:
            if not selected.get(k, None):
                selected[k] = {}
            selected = selected[k]

    def _delete_key(self, keys: list) -> None:
        "Takes a list of keys and deletes the last key"
        selected = self.data_stream
        for k in keys[:-1]:
            selected = selected[k]
        selected.pop(keys[-1])

    def toggle_channel_action(self, channel: int, action: str, guild_id: int):
        "Sets or removes a channel for an action. Returns False if the channel is removed, returns True if the channel is added"
        self._iterate_keys(['channels', action, guild_id])
        if channel in self.get_channels_for_action(action, guild_id):
            self.data_stream['channels'][action][guild_id].remove(channel)
            self._save()
            return False
        else:
            if self.data_stream['channels'][action][guild_id] == {}:
                self.data_stream['channels'][action][guild_id] = [channel]
            else:
                self.data_stream['channels'][action][guild_id].append(channel)
            self._save()
            return True

    def get_channels_for_action(self, action: str, guild_id: int):
        "Gets the channels that an action is assigned to"
        self._iterate_keys(['channels', action])
        return self.data_stream['channels'][action].get(guild_id, None)

    def add_banned_tags(self, tags: list, guild_id: int) -> list:
        "adds tags to banned list for the guild"
        guild_id = str(guild_id)
        self._iterate_keys(['banned_tags'])
        self.data_stream['banned_tags'][guild_id] = self.get_banned_tags(guild_id) + tags
        self._save()
        return self.data_stream['banned_tags'][guild_id]
    
    def remove_banned_tags(self, tags: list, guild_id: int) -> list:
        "removes tags from banned list for the guild"
        guild_id = str(guild_id)
        self._iterate_keys(['banned_tags'])
        if not self.data_stream['banned_tags'].get(guild_id):
            return []
        for tag in tags:
            self.data_stream['banned_tags'][guild_id].remove(tag)
        return self.data_stream['banned_tags'][guild_id]
    
    def get_banned_tags(self, guild_id: int) -> list:
        "gets the banned tags for a guild"
        guild_id = str(guild_id)
        self._iterate_keys(['banned_tags'])
        return self.data_stream['banned_tags'].get(guild_id, [])